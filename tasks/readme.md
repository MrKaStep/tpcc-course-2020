# Обязательные задачи

**ДЗ** | **Задача**
--- | ---
0-intro | deadlock
0-intro | dining
1-mutex | spinlock
1-mutex | tricky
1-mutex | try-lock
2-condvar | barrier
2-condvar | semaphore
